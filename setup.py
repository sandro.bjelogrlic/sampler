import setuptools
import os


base_packages = ["scikit-learn>=0.20.2",
                 "pandas>=0.25",
                 "matplotlib==3.1.1"
                 ]

try:
    if os.environ.get('CI_COMMIT_TAG'):
        version = os.environ['CI_COMMIT_TAG']
    else:
        version = os.environ['CI_JOB_ID']
except:
    version = 'local'


setuptools.setup(
    name='sample_split',
    version=version,
    description='',
    author='Sandro Bjelogrlic',
    author_email='sandro.bjelogrlic@gmail.com',
    license='',
    packages=setuptools.find_packages(),
    package_data={'pyrisk': ['datasets/data/*.pkl']},
    install_requires=base_packages,
    url='https://gitlab.com/sandro.bjelogrlic/sample_split',
    zip_safe=False
)
