import numpy as np
from sklearn import model_selection as ms


def train_test_split(X, y, test_size=0.25, random_state=None, grouping_column=None):
    """
    Performs train test split by taking into account a column to use for mapping the slplits
    Args:
        X: pd.DataFrame with features. It must contain also the grouping column
        y: pd.Series: targets
        test_size float: fraction of dataset to use for test
        random_state: random seed
        grouping_column: Column to use for mapping the groups. If none, it uses the standard sklearn train_test_split
    """
    if grouping_column is None:
        return ms.train_test_split(X, y, test_size=test_size, random_state=random_state)

    np.random.seed(random_state)
    # Find all the unique values
    unique_values = np.unique(X[grouping_column].values)

    total_uniques = unique_values.shape[0]

    # True means test, False means train
    random_mask = np.random.random_sample(total_uniques) <= test_size

    map_dict = {uv: rm for uv, rm in zip(unique_values, random_mask)}

    mask_series = X[grouping_column].map(map_dict)

    X_train = X[~mask_series]
    X_test = X[mask_series]
    y_train = y[~mask_series]
    y_test = y[mask_series]

    return X_train, X_test, y_train, y_test

